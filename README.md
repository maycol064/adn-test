# Mutaciones ADN - Prueba técnica para puesto de desarrollador full stack.

### Desarrollado por Maycol Flores

## Pasos para ejecutarlo de manera local:
1. Tener Node.js y NPM instalados en el equipo
2. Clonar el repositorio
3. Navegar a la carpeta del proyecto
4. Ejectuar el comando:
    ```bash 
    $ npm install
    ```
    Este comando instalará los módulos necesarios
5. Ejecutar el commando:
    ```bash
    $ npm run build
    ```
    Este comando construirá la versión de producción
6. Ejecutar el comando:
    ```bash
    $ npm run start
    ```
    Es comando levantará el servidor de manera local
7. Para acceder de manera local ingrese mediante el localhost: localhost:3000/mutation o http://127.0.0.1:300/mutation


## API en producción
La API está desplegada en un servidor de Render, tiene un almacenamiento persistente con una base de datos de MongoDB desplegada en un clouster de Atlas. Para poder acceder a la versión de producción ingresar al link: https://dna-mutation.onrender.com/mutation