import { Request, Response } from 'express';
import { validateDNAArray } from '../helpers/validateDNAArray';
import { dnaModel } from '../models/ADNModel';
import { hasMutation } from '../mutationDNA';

export const postDNA = async (req: Request, res: Response) => {
  if (!req.body) return res.status(400).json({ message: 'No se recibieron datos' });
  try {
    const { dna } = req.body;
    const isValid = validateDNAArray(dna);
    if (!isValid) return res.status(400).json({ message: 'Data incorrecta, verifique que corresponda a la secuencia de ADN.' }).status(400);
    const mutation = hasMutation(dna);
    const exists = await dnaModel.findOne({ dna });
    if (exists) {
      return res.status(400).json({ message: 'ADN ya fue ingresado, intente con uno nuevo.' });
    } else {
      const response = await dnaModel.create({ dna, mutation: mutation });
      if (mutation) {
        return res.status(200).json({ message: response });
      } else {
        return res.status(403).json({ message: response });
      }
    }
  } catch (error) {
    return res.status(500).json({ message: error });
  }
}

export const getInfo = async (req: Request, res: Response) => {
  try {
    const dnas = await dnaModel.find();
    const size = dnas.length;
    const mutations = dnas.filter(dna => dna.mutation).length
    const noMutations = dnas.filter(dna => !dna.mutation).length
    const ratio = mutations / size;
    res.json({ count_mutations: mutations, count_no_mutations: noMutations, ratio }).status(200);
  } catch (error) {
    res.json({ message: error }).status(500);
  }
}