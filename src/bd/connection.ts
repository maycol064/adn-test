import mongoose from 'mongoose';
import dotenv from 'dotenv';

dotenv.config();

const URI = process.env.URI_MONGODB || '';

export const initConnection = async () => {
  try {
    await mongoose.connect(URI);
    console.log('Connected with MongoDB');
  } catch (error) {
    console.log(error);
    return error;
  }
}