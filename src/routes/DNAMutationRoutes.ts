import { Router } from 'express';
import { getInfo, postDNA } from '../controllers/DNAMutationController';

const router = Router();

router.post('/', postDNA);
router.get('/', getInfo);

export default router;