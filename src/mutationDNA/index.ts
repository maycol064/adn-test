
const checkRow = (row: string): boolean => /AAAA|TTTT|CCCC|GGGG/.test(row);;

const checkColumn = (dna: string[], n: number, col: number): boolean => {
  let column = "";
  for (let i = 0; i < n; i++) {
    column += dna[i][col];
  }
  return /AAAA|TTTT|CCCC|GGGG/.test(column);
}

const checkDiagonal = (dna: string[], n: number, row: number, col: number, direction: number): boolean => {
  let diagonal = "";
  while (row >= 0 && row < n && col >= 0 && col < n) {
    diagonal += dna[row][col];
    row += 1;
    col += direction;
  }
  return /AAAA|TTTT|CCCC|GGGG/.test(diagonal);
}

export const hasMutation = (dna: string[]): boolean => {
  const n = dna.length;
  for (let i = 0; i < n; i++) {
    if (checkRow(dna[i]) || checkColumn(dna, n, i)) {
      return true;
    }
  }
  for (let i = 0; i < n; i++) {
    if (checkDiagonal(dna, n, i, 0, 1)) {
      return true;
    }
  }
  for (let i = 0; i < n; i++) {
    if (checkDiagonal(dna, n, i, n - 1, -1)) {
      return true;
    }
  }
  return false;
}