import express, { Request, Response } from 'express';
import dotenv from 'dotenv';
import { initConnection } from './bd';
import dnaRouter from './routes/DNAMutationRoutes';

const app = express();
dotenv.config();

const port = process.env.PORT || 3000;

app.use(express.json());

app.get('/', (req: Request, res: Response) => res.json({ message: 'Todo ok!' }));

app.use('/mutation', dnaRouter);

initConnection()
  .then(() => {
    app.listen(port, async () => {
      console.log(`Server is runing in ${port}`)
    });
  })
  .catch((err) => console.log(err));