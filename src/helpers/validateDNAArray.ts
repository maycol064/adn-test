export const validateDNAArray = (dna: string[]) => {
  const validChars = ["A", "C", "G", "T"];

  for (const sequence of dna) {
    if (dna.length !== sequence.length) {
      return false;
    }
    if (sequence.indexOf(" ") !== -1) {
      return false;
    }
    for (const char of sequence) {
      if (!validChars.includes(char)) {
        return false;
      }
    }
  }

  return true;
}