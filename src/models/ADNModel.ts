import { Schema, model } from "mongoose";

export interface DNA {
  dna: string[];
  mutation: boolean;
};

const dnaSchema = new Schema<DNA>({
  dna: { type: [String], required: true },
  mutation: { type: Boolean, required: true }
});

export const dnaModel = model<DNA>('DNA', dnaSchema);